# -*- coding: utf-8 -*-
# %matplotlib inline

# # Waveform Inversion
#
# (Full)-Waveform Inversion (FWI) can reveal the properties of complex media that are otherwise not accessible to direct observation. This is based on measurements of mechanical waves - excited by either active or passive sources - that propagate through an object of interest and for which we record time series at certain receiver locations.
# Those data contain valuable information about the object's interior that we can use to create quantitative images of its material properties.
#
# In this tutorial, we consider a fairly simple setup in 2D, which is inspired by typical apertures in ultrasound computed tomography (USCT) to illuminate human tissue.

# +
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr

import salvus.namespace as sn


# -

# ## Step 1: Generate the domain and ground truth
#
# This is a purely synthetic study, so we have to generate the "measurements" ourselves using a model with a few inclusions. We use a simple box domain of `20 x 20 cm` centered around the origin and insert an object with two spherical inclusions that mimics a breast phantom in a USCT acquisition. The model for density (`RHO`) and speed of sound (`VP`) is created from a structured grid.

def get_spherical_inclusion():
    nx, ny = 200, 200
    x = np.linspace(-0.1, +0.1, nx)
    y = np.linspace(-0.1, +0.1, nx)
    xx, yy = np.meshgrid(x, y, indexing="ij")

    # Add 3 spherical inclusions
    vp = 1500.0 * np.ones_like(xx)
    rho = 980.0 * np.ones_like(xx)
    mask = np.sqrt(xx ** 2 + yy ** 2) < 0.05
    vp[mask] = 1480.0
    rho[mask] = 1000.0

    mask = np.sqrt(xx ** 2 + (yy-0.025) ** 2) < 0.015
    vp[mask] = 1550.0
    rho[mask] = 1040.0

    mask = np.sqrt(xx ** 2 + (yy+0.025) ** 2) < 0.015
    vp[mask] = 1460.0
    rho[mask] = 1010.0

    ds = xr.Dataset(
        data_vars={
            "vp": (["x", "y"], vp),
            "rho": (["x", "y"], rho),
        },
        coords={"x": x, "y": y},
    )

    return ds


# Let's take a look at the model.

# +
true_model = get_spherical_inclusion()

# Plot the xarray dataset.
plt.figure(figsize=(16, 6))
plt.subplot(121)
true_model.vp.T.plot()
plt.subplot(122)
true_model.rho.T.plot()
plt.suptitle("Model with spherical inclusions")
plt.show()

# -

# This is also the starting point for setting up a project for the inversion. We can directly create the project from the model defined above. We will call this model the `true_model`.

# +
# !rm -rf fwi_project

vm = sn.model.volume.cartesian.GenericModel(
    name="true_model", data=true_model
)

proj = sn.Project.from_volume_model(path="fwi_project", volume_model=vm)
# -

# ## Step 2: Define the acquisition geometry
#
# We assume a ring-shaped aperture with ultrasound transducers surrounding the breast phantom.
# To keep the computations cheap, we will use only 5 emitters and 100 receiving transducers which are the same for every emitter. The `simple_config` has a few built-in options to create lists of sources and receivers, which we want to make use of. By defining the two rings below - one for sources and one for the receivers, we can readily create an `EventCollection`, i.e., a number of experiments characterized by the locations of the emitting and the receiving transducers.

# +
srcs = sn.simple_config.source.cartesian.collections.ScalarPoint2DRing(
    x=0, y=0, radius=0.09, count=5, f=1.0
)

recs = sn.simple_config.receiver.cartesian.collections.RingPoint2D(
    x=0, y=0, radius=0.09, count=100, fields=["phi"]
)

proj += sn.EventCollection.from_sources(sources=srcs, receivers=recs)
# -

# The transducers are identical except for their location, so we use the same simulation time of `1.5 ms` (and later on also the same source time function) for each of them. Here, we also fix the time step to `400 ns`. While this is not strictly necessary and could be automatically detected, of course, it will simplify the data comparison across different simulations.

wst=sn.WaveformSimulationTemplate(end_time_in_seconds=0.00015)
wst.physics.wave_equation.time_step_in_seconds = 4.0e-7

# Now, we put everything together to configure the simulations for the ground truth. To keep the costs low, we only consider a center frequency of `50 kHz` and a mesh accurate up to `100 kHz`.

# +
proj += sn.SimulationConfiguration(
    name="true_model_100kHz",
    #
    # Settings that influence the mesh.
    elements_per_wavelength=2,
    tensor_order=4,
    max_frequency_in_hertz=100000.0,
    #
    model_configuration=sn.ModelConfiguration(
        background_model=None, volume_models="true_model"
    ),

    # Potentially event dependent settings.
    event_configuration=sn.EventConfiguration(
        waveform_simulation_template=wst,
        wavelet=sn.simple_config.stf.Ricker(center_frequency=50000.0),
    )

)
# -

# This added 5 events (aka experiments or scans) to our project. We can take a look at the geometry of each of the experiments by just querying the events from the project. The first tab shows the full geometry of all source and receiver locations. On the second tab, you can scroll through the apertures for each individual experiment.

proj.get_events()

# Now it is time for the first simulation in our project. This is only to create data though. We haven't even started with the inversion yet...

proj.launch_simulations(
    simulation_configuration="true_model_100kHz",
    events=proj.get_events(),
    site_name="local",
    ranks_per_job=4,
)

# Once the simulations are finished, we can query the data and visualize it directly in the notebook. Note that you might have to execute this cell again, in case the simulations were not already finished.

true_data = proj.get_data(
    data_name="true_model_100kHz",
    events=proj.get_events()
)

# Similar to the event geometry, we can now slide through the individual recordings of each event.

true_data

# ## Step 3: Create the initial model
#
# Alright, enough of these simulations. Now, it is actually time to focus on the inversion.
#
# **How do we start?** Well, we need a model of course.
#
# **What do we already know about the medium?** Let's pretend we haven't seen the figures above and have no idea about the phantom. The best prior knowledge we have is that the domain is filled with water and we assume that the speed of sound is `1500 m/s` and the density is `980 kg/m^3`.
#
# We create a homogeneous background model and set up a simulation configuration that contains the same events as the true data.

# +
bm = sn.model.background.homogeneous.IsotropicAcoustic(vp=1500.0, rho=980.0)
mc = sn.ModelConfiguration(background_model=bm)

proj += sn.SimulationConfiguration(
    name="initial_model",
    #
    # Settings that influence the mesh.
    elements_per_wavelength=2,
    tensor_order=2,
    max_frequency_in_hertz=100000.0,
    #
    model_configuration=mc,

    # Potentially event dependent settings.
    event_configuration=sn.EventConfiguration(
        waveform_simulation_template=wst,
        wavelet=sn.simple_config.stf.Ricker(center_frequency=50000.0),
    )

)
# -

# ## Step 4: Compute synthetics
#
# With the model in hand, we could start iterating right away if we were brave enough. However, in all relevant cases we would want to do some quality checks first before burning many CPU hours. So let's start by just looking at the synthetic waveforms that the initial model produces.

proj.launch_simulations(
    simulation_configuration="initial_model",
    events=proj.get_events(),
    site_name="local",
    ranks_per_job=4,
)

synthetic_data = proj.get_data(
    data_name="initial_model",
    events=proj.get_events()
)

synthetic_data

# This looks alright, and we are moving on to the next step where we actually initialize the inverse problem.
#
# ## Step 5: Initialize the inverse problem
#
# The inverse problem is a similar entity like all the other configuration objects. We want to be able to keep track of all the individual steps, and avoid repeating the same task more than once.
#
# We need to specify the prior, which is just the `SimulationConfiguration` object of the initial model we created above. Furthermore, we need to specify all possible events that we might consider during the inversion. This could be a subset of events defined in the project, and we could add more events later on.
# Together with the events, we need to pass the observed data. Because we created it synthetically, this is also just a `SimulationConfiguration` object. The remaining parameters specify which parameters to invert for (`VP` and `RHO`), what misfit functional to use, and where to run the simulations.

proj += sn.InverseProblemConfiguration(
        name="my inversion",
        prior_model="initial_model",
        events=proj.get_events(),
        observed_data="true_model_100kHz",
        inversion_parameters = ["VP", "RHO"],
        misfit_config="L2",
        site_name="local",
        ranks_per_job=2,
)

# To keep track of the inversion, this object will also be serialized to disk, so we load it again at a later stage. _This is not fully functional yet!_ Let's check the status of the inverse problem, when we retrieve from the project.
# Not surprisingly, there is almost no information there yet, but at least we see the initial model and the parameterization.

ip = proj.get_inverse_problem(name="my inversion")
ip.state

# ## Step 6: Manually experiment with misfits and gradients
#
# Before triggering the iterations, most applications necessitate at least some amount of quality control, which makes it inevitable to run a few simulations manually. Let start by computing some misfits for the initial model.
#
# **Wait!**
#
# Do we actually need to run the simulations again? We have already computed shot gathers for the initial model, haven't we? Let's see what the function call will do.

misfits, adjoint_sources = ip.compute_misfits(model_name="initial_model",  events=proj.get_events())
print(misfits)

# Hm. We did not include any output in the previous run, but since we are now expecting to run an adjoint simulation soon, we had to rerun the forward simulations to store some checkpoints.
#
# The cell above returned the misfits for all individual events. Maybe it is more convenient to sum them directly?

misfit = ip.compute_and_sum_misfits(model_name="initial_model")
print(misfit)

# If you closely study the output, you will notice that we did not repeat any simulations this time, but just queries the precomputed values.
#
# What's next? Sensitivities!

gradients = ip.compute_gradients(model_name="initial_model", events=proj.get_events())

# This function returns individual gradients, which can be useful for event-dependent or batch inversions.
# Again, take a close look at the verbose output. The forward runs were cached and not repeated to obtain the gradients.
#
# Even better, in 2D you can immediately visualize them in a widget.

gradients

# Similar to the misfits, we can also obtain the summed gradient over all selected events in one go.

gradient = ip.compute_and_sum_gradients(model_name="initial_model", events=proj.get_events())
gradient

# There is still quite a strong imprint of the source locations in the gradient. Some smoothing will help to obtain better updates. This is encapsulated in the concept of a preconditioner, that we will also pass on to the optimization method.

# +
precon = sn.IsotropicSmoothing(smoothing_length_in_meters=0.005,
                                      site_name='local',
                                      ranks_per_event=2)

smoothed_gradient = precon.apply(ip.model_gradients['initial_model'])
smoothed_gradient.model
# -

# Smooth. Feel free to play around with the smoothing length to see the effects on the gradient.
#
#
# There is a zoo of non-linear optimization methods and yet, we will use the most basic one,
# which is steepest descent. To make it at least slightly more fancy, we apply the Armijo condition to determine the step length and pass on the preconditioner to smooth the gradients internally, before a model update is proposed.

# ## Step 7: Update the model

opt = sn.SteepestDescent(
    step_length_rule = sn.Armijo(),
    max_relative_model_perturbation=0.01,
    preconditioner=precon,
    debug=False)
ip.set_method(opt)

# This hasn't done anything yet. We have just settled on a method to compute a first model update.

ip.do_iteration()

# The step length appeared to be too large at the beginning, but has been adjusted until we finally found a model that reduces the misfit. Let's check  the `state` again. Quite a lot has happened in the meantime and the information is piling up.

ip.state

# A lot more interesting than all these numbers is to check out how the updated model looks like. Are you ready?

ip.models[ip.get_current_model_name()].model

# There is still quite a strong imprint of the source locations, but we start moving into the right direction.
# The high velocity inclusion in the top part also shows up in the density update.

# ## Step 8: Automate
#
# The first update gave us confidence in the setup. For the moment, our work here is done.
# Let's run a few more iterations, lean back and wait for the results.
#
# We just pass a `count=5` to the `do_iteration` function and it will perform 5 model updates.

ip.do_iteration(count=5)

# Done!
#
# Did we converge towards the ground truth?

ip.models[ip.get_current_model_name()].model

# Well, at least we were getting closer and the heterogeneities start to form. You can, of course, continue with more iterations or play around with the settings.
#
#
# That's the end of this tutorial. We ran a few iterations of full-waveform inversion in a typical aperture for ultrasound screenings. Note that although the problem size is small and we did not apply any sophisticated inversion technique, we were able to create a suitable initial model and to perform a few model update with just a few lines of code. Nothing would change really if we applied this at a larger scale. By changing the site name to a remote site with more compute capabilities, we could easily scale up the problem.
#
# ## Bonus: Additional prior knowledge
#
# In a typical USCT setup, there is always enough space between the ultrasound transducers and the phantom.
# What if we include that information as prior knowledge into our problem formulation?
#
# An easy way of doing this, is to define a region of interest and restrict the reconstruction to this area.
#
# To keep it simple, we just define a sphere with a radius of `6.5 cm` as the target region.

m = proj.get_mesh("initial_model")
region_of_interest = np.zeros_like(m.elemental_fields["VP"])
mask = np.linalg.norm(m.get_element_nodes(),axis=2) < 0.065
region_of_interest[mask] = 1.0
m.attach_field("region_of_interest", region_of_interest)

# Let's see if this helps with the iterations. To be able to compare the results, we just create a new inverse problem within the same project, initialize the region of interest, and start iterating.

proj += sn.InverseProblemConfiguration(
        name="my second inversion",
        prior_model="initial_model",
        events=proj.get_events(),
        observed_data="true_model_100kHz",
        inversion_parameters = ["VP", "RHO"],
        misfit_config="L2",
        site_name="local",
        ranks_per_job=2,
)

ip2 = proj.get_inverse_problem(name="my second inversion")
ip2.set_region_of_interest(m)
ip2.set_method(opt)
ip2.do_iteration()

# Let's see if the region of interest was considered when the model was updated.

ip2.models[ip2.get_current_model_name()].model

# Indeed, outside of the pre-defined sphere, the model is still constant and has the same values as the initial model.
#
# Let's do a few more iterations and see what the reconstruction will be.

ip2.do_iteration(count=5)

ip2.models[ip2.get_current_model_name()].model

# This looks better, so the prior knowledge was indeed helpful.

# **Bonus question.** The setup above is also a prime example of an "inverse crime", except for one small detail.
# Can you identify what it is?


