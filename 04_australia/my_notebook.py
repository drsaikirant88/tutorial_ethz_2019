# # Continental scale simulations
#
# ![moho_crust20.jpg](moho_crust20.jpg)

# Continental scale simulations and inversions form the bulk of what we do in the SWP group. In this tutorial we'll set up, from scratch, a 3-D spherical domain for the region surrounding Australia. This domain will include a 3-D crustal and mantle model, taken from [AusREM](http://rses.anu.edu.au/seismology/AuSREM/index.php), as well as 3-D surface and Moho topography. After our model and simulations are set up, we'll fire them off to a remote site (if available) in order to run simulations accurate to a minimum period of 40 seconds. We'll then gather the data from the remote site and apply some basic processing functions to some observed, as well as our synthetic, data. All right -- let's get started!

import pathlib
from salvus import namespace as sn

# # Loading a project
#
# Contrary to previous tutorials, in this tutorial we'll open a project which already exists on disk. Before beginning this notebook you should have run a small script in this directory called `setup_project.py`. This will initialize a small domain centered on Australia, and download real data via ObsPy for a few stations from the `IU` global network. These are high quality stations which will be perfect for a comparison to our synthetic data. The line below allows us to simply load a project from an existing path on disk.

p = sn.Project("./proj")

# # Adding volumetric models
#
# In previous tutorials we outlined the difference between background and volumetric models, and until now have only considered background models. Here we'll foray into the use of volumetric models as well, and we'll specifically choose a different model for both the crust and the mantle. In the tutorial folder you should see two NetCDF (`.nc`) files entitled `ausrem_crust.nc` and `ausrem_mantle.nc`. The name of these models are relatively self descriptive: they are modified version of the AusREM (Australian Seismological Reference Model) crust and mantle models respectively. The format that they are stored in is governed by the [CF conventions](http://cfconventions.org/), which is a commonly used format for storing geoscientific data.
#
# Since we're focussed here on Australia we'll shift back to AusREM for now. In a seismological context, we can restrict the interpolation of models to specific regions, such as the crust or mantle. Also, in many cases we want to apply a taper to the volumetric models so that they will smoothly blend into the background model. To accomplish this, the seismological model API allows us to optionally specify a `taper_in_degrees` parameter. Ok, now let's add the crustal model to the project...

p += sn.model.volume.seismology.CrustalModel(
    name="ausrem_crust", data="./data/ausrem_crust.nc", taper_in_degrees=5.0
)

# ...and now the mantle model...

p += sn.model.volume.seismology.MantleModel(
    name="ausrem_mantle", data="./data/ausrem_mantle.nc", taper_in_degrees=5.0
)

# ...and now finally let's create a complete model configuration using anisotropic PREM as a background model.

mc = sn.ModelConfiguration(
    background_model="prem_ani_one_crust",
    volume_models=["ausrem_crust", "ausrem_mantle"],
)

# ## Adding surface and Moho topography
#
# Surface and Moho topography can be added in a similar fashion to those on Cartesian domains.
#
# These files contain filtered versions of global surface and moho topography.

p += sn.topography.spherical.SurfaceTopography(
    name="topo_surface", data="./data/topography_wgs84_filtered_512.nc"
)

p += sn.topography.spherical.MohoTopography(
    name="topo_moho", data="./data/moho_topography_wgs84_filtered.nc"
)

tc = sn.topography.TopographyConfiguration(
    topography_models=["topo_surface", "topo_moho"]
)

# ## Preparing the simulation configuration
#
# To finalize our simulation configuration, we first prepare an event configuration to define the time and frequency axes. We'll simulate half an hour of synthetic waveforms, and take as our source time function a Gaussian in moment-rate. In this case we'll set a very short half-duration in seconds to get an almost white spectrum in our synthetic data. We'll use this to motivate the processing functions we later apply.

ec = sn.EventConfiguration(
    wavelet=sn.simple_config.stf.GaussianRate(half_duration_in_seconds=1.5000),
    waveform_simulation_template=sn.WaveformSimulationTemplate(
        end_time_in_seconds=1800.0
    ),
)

# As always, we need to also set up some absorbing boundary parameters here. At this point it is fair to ask the question: how do I choose a reference velocity in a domain with a 3-D velocity model, and what even is the reference frequency if my spectrum is white? The answer to both of these questions is that, as always, the performance of the absorbing boundaries is a tunable parameter. In the case of a white spectrum a good rule is to set the reference frequency to the dominant frequency in your observed data.

ab = sn.AbsorbingBoundaryParameters(
    reference_velocity=6000.0,
    number_of_wavelengths=3.5,
    reference_frequency=1 / 40.0,
)

# ## Running a simulation
#
# We've now got everything we need to set up our simulation configuration object, as we do in the next cell below. Here we'll generate a simulation mesh which ensures that we have at least 1 element per wavelength using a minimum period of 40 seconds, and the using anisotropic PREM as a size function.

# Combine everything into a complete configuration.
p += sn.SimulationConfiguration(
    tensor_order=1,
    name="40seconds",
    elements_per_wavelength=1.0,
    min_period_in_seconds=40,
    max_depth_in_meters=1000e3,
    model_configuration=mc,
    topography_configuration=tc,
    event_configuration=ec,
    absorbing_boundaries=ab,
)

# We can, as is true with the other domains, visualize our new simulation configuration...

p.nb_viz("40seconds", events=p.get_events())

# ... and then go ahead and run our simulation. The time taken to complete will vary significantly depending on the options you've chosen above. For instance, adding both Moho and surface topography decreases the time step by quite a bit due to the relatively small elements in the oceanic crust.

p.launch_simulations(
    ranks_per_job=60,
    site_name="daint",
    events=p.get_events(),
    wall_time_in_seconds_per_job=3600,
    simulation_configuration="40seconds",
)

# ## Analyzing output data
#
# It is usually necessary to process files to be able to compare data to synthetics.

p.query_simulations()

p += sn.processing.seismology.BasicBandpassRemoveResponseProcessingConfiguration(
    name="basic_processing",
    data_source_name="EXTERNAL_DATA:raw_recording",
    bandpass_minimum_frequency=1.0 / 100.0,
    bandpass_maximum_frequency=1.0 / 40.0,
    pre_filter=[0.002, 0.004, 1, 2],
    units="velocity",
)

p += sn.processing.seismology.BasicBandpassProcessingConfiguration(
    name="40_seconds",
    data_source_name="40seconds",
    bandpass_minimum_frequency=1.0 / 100.0,
    bandpass_maximum_frequency=1.0 / 40.0,
)

p.nb_compare("PROCESSED_DATA:basic_processing", ["PROCESSED_DATA:40_seconds"])

# ## Using other models
#
# The `.nc` files we used in this tutorial are in fact the same convention that is used by the IRIS Earth Model Catalogue (EMC). This means that many of the models stored in the EMC can be downloaded and used
# directly in Salvus! Unfortunately, there is no standard parameter set stored in the EMC, and we've found that not all the files _actually_ conform to the required convention. Nevertheless, with some minimal processing, you can have the public library of 3-D Earth models at your disposal to run simulations through. The picture below show a successful attempt at including 3 distinct 3-D Earth models (crust, mantle, cascadia) for a region along the US west coast.
#
# http://ds.iris.edu/ds/products/emc/
#
# ![](./na.png)

# Adding the downloaded file as a mantle model.
p += sn.model.volume.seismology.MantleModel(
    name="SEMUM", data="./SEMUM_kmps.nc"
)

# Combine into a model configuration.
mc1 = sn.model.ModelConfiguration(
    background_model="prem_iso_one_crust", volume_models=["SEMUM"]
)

# Combine everything into a complete configuration.
p += sn.SimulationConfiguration(
    tensor_order=1,
    name="40seconds_semum",
    elements_per_wavelength=1.0,
    min_period_in_seconds=40,
    max_depth_in_meters=1000e3,
    model_configuration=mc1,
    topography_configuration=tc,
    event_configuration=ec,
    absorbing_boundaries=ab
)

# Visualize the setup.
p.nb_viz("40seconds_semum", events=p.get_events())

# Can also open it in Paraview.
m = p.get_mesh_filenames("40seconds_semum")["xdmf_filename"]
# !open {m}
