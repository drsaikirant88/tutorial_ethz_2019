from salvus import namespace as sn

# Define a spherical chunk domain.
lat_c, lon_c = -27.5, 135
lat_e, lon_e = 25.5, 42.0
d = sn.domain.dim3.SphericalChunkDomain(
    lat_center=lat_c,
    lat_extent=lat_e,
    lon_center=lon_c,
    lon_extent=lon_e,
    radius_in_meter=6371e3,
)

# Initialize the project from a domain.
p = sn.Project.from_domain(path="proj", domain=d)

# Add an event
e = sn.Event(
    sources=sn.simple_config.source.seismology.parse(
        "./event.txt", dimensions=3
    ),
    receivers=[],
)

p += e

sn.project_actions.download_seismological_data_for_event(
    data_name="raw_recording",
    project=p,
    event=p.get_events()[0],
    add_receivers_to_project_event=True,
    receiver_fields=["velocity"],
    network="IU",
    download_providers=["IRIS"],
)
